"  *  GENERAL

"  ** Activate true color mode.
set termguicolors

"  ** Set application theme.
colorscheme ayu

"  ** Set font & size.
GuiFont Hack:h15

"  ** Left padding column same as background.
hi FoldColumn guibg=none

"  ** Hide tildes for empty lines.
hi EndOfBuffer guifg=bg

"  ** GUI specific mappings.
source ${XDG_CONFIG_HOME}/nvim/mapping/gui.vim
