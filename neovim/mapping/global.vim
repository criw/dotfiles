"  *  GENERAL

"  ** Navigation between panes.
nmap <silent> <C-k> <C-W>k
nmap <silent> <C-j> <C-W>j
nmap <silent> <C-h> <C-W>h
nmap <silent> <C-l> <C-W>l

"  ** Use arrows as resize.
nnoremap  <Up>    :resize +2<CR>
nnoremap  <Down>  :resize -2<CR>
nnoremap  <Left>  :vertical :resize +2<CR>
nnoremap  <Right> :vertical :resize -2<CR>

"  ** Copy from vim to clipboard.
vnoremap  <leader>y  "+y
nnoremap  <leader>y  "+y

"  ** Paste from somewhere to vim.
nnoremap <leader>p "+p
nnoremap <leader>P "+P
vnoremap <leader>p "+p
vnoremap <leader>P "+P

"  ** Open a terminal emulator.
map <Leader>c :split term://zsh<CR>

"  ** Edit configuration file.
map <Leader>te  :vsp ~/.config/nvim/init.vim<CR>

"  ** Reload configuration file.
map <Leader>tr  :source ~/.config/nvim/init.vim<CR>

"  ** Edit gui configuration file.
map <Leader>ge :vsp ~/.config/nvim/ginit.vim<CR>

"  ** Reload gui configuration file.
map <Leader>gr :source ~/.config/nvim/ginit.vim<CR>

"  ** Render rmarkdown editing file.
map <F5> :!echo<space>"require(rmarkdown);<space>render('<c-r>%')"<space>\|<space>R<space>--vanilla;pkill<space>-HUP<space>mupdf<enter>


"  *  PLUGINS

"  ** Expand snippets			(neosnippet)
imap <M-TAB> <Plug>(neosnippet_expand_or_jump)
smap <M-TAB> <Plug>(neosnippet_expand_or_jump)
xmap <M-TAB> <Plug>(neosnippet_expand_target)

"  ** Enable/disable git diffs.		(vim-gitgutter) 
map <Leader>gt :GitGutterToggle<CR>

"  ** Enable/disable nerdTree.		(nerdtree)
map <Leader>nt :NERDTreeToggle<CR>

"  ** Enable distraction free mode.	(goyo.vim) 
map <Leader>do :Goyo 80x40<CR>

"  ** Disable distraction free mode.	(goyo.vim)
map <Leader>dd :Goyo!<CR>
