"  *  GENERAL

if g:colors_name == "ayu"
	"  ** Set the dark color scheme
	map <Leader>Td :let ayucolor="dark"<CR><Leader>gr

	"  ** Set the light color scheme
	map <Leader>Tl :let ayucolor="light"<CR><Leader>gr
endif
