"  *  GENERAL

"  ** Plugins & related configuration.
so ${XDG_CONFIG_HOME}/nvim/plugins.vim

"  ** Not vi friendly.
set nocompatible

"  ** Set leader key.
let mapleader = ','

"  ** Set vertical splits to expand to right.
set splitright

"  ** Set horizontal split to expand down.
set splitbelow

"  ** Hide vertical bar split colors.
hi VertSplit cterm=NONE ctermbg=NONE guibg=NONE

"  ** Left padding size.
set foldcolumn=1

"  ** Left padding column same as background.
hi  FoldColumn ctermbg=none

"  ** Tabulations based on \t & 8 spaces.
set tabstop=8
set softtabstop=8
set shiftwidth=8
set noexpandtab

"  ** Set paths autocompletation based on current file path.
set autochdir

"  ** Global mappings.
so ${XDG_CONFIG_HOME}/nvim/mapping/global.vim

"  ** Terminal specific mappings.
so ${XDG_CONFIG_HOME}/nvim/mapping/terminal.vim

"  ** Start terminal emulator in insert mode.
autocmd TermOpen * startinsert
