call plug#begin('~/.config/nvim/plugins')
  "  ** Cool status bar.
  Plug 'maciakl/vim-neatstatus'

  "  ** Autocompletation tool.
  Plug 'Shougo/deoplete.nvim', { 'do': ':UpdateRemotePlugins' }

  "  ** Snippets tool.
  Plug 'Shougo/neosnippet.vim'

  "  ** Git integration in vim
  Plug 'tpope/vim-fugitive'

  "  ** Git diff tool.
  Plug 'airblade/vim-gitgutter'

  "  ** Nerd tree.
  Plug 'scrooloose/nerdtree'

  "  ** Distraction free mode tool.
  Plug 'junegunn/goyo.vim'

  "  ** Themes.
  Plug 'ayu-theme/ayu-vim'
  Plug 'drewtempelmeyer/palenight.vim'
  Plug 'arcticicestudio/nord-vim'
call plug#end()

"  *  CONFIGURATION

"  ** Enable at startup.		(deoplete.nvim)
let g:deoplete#enable_at_startup = 1

"  ** Disable predefined snippets.	(neosnippet.vim)
let g:neosnippet#disable_runtime_snippets = { '_': 1 }

"  ** Set snippets directory.		(neosnippet.vim)
let g:neosnippet#snippets_directory = '~/.config/nvim/snippets' 
